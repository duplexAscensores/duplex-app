const INITIALSTATE = {
  appName: 'Duplex Elevacion',
  currentUrl: '/home',
  offLineApp: false
};

export const reducer = (state = INITIALSTATE, actions) => {
  switch(actions.type) {
    case 'CURRENTURL':
      return { 
        ...state,
        currentUrl: actions.url
      }

    case 'OFFLINEAPP':
      return {
        ...state,
        offLineApp: actions.offLineApp
      }
    
    default:
      return state;
  }
};