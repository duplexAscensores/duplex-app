export const navigate = (url) => {
  return  {
    type: 'CURRENTURL',
    url
  }
};

export const offLineApp = (offLineApp) => {
  return {
    type: 'OFFLINEAPP',
    offLineApp
  }
};