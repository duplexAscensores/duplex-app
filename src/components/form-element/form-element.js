import { LitElement, html, css } from 'lit-element';
import { style } from './form-element-style';
class FormElement  extends LitElement {

  static get styles() {
    return [
      style
    ]
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <section class="wrapper-form">
        <form>
          
        </form>
      </section>
    `;
  }
}

customElements.define('form-element', FormElement);