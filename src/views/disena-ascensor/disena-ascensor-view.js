import { LitElement, html } from 'lit-element';
import { style } from './disena-ascensor-style';

import { shareStyle } from '../../../node_modules/duplex-theme/src/duplex-theme';
class DisenaAscensorView  extends LitElement {

  static get styles() {
    return [
      shareStyle,
      style
    ];
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  render() {
    return html`
      
    `;
  }
}

customElements.define('disena-ascensor-view', DisenaAscensorView);