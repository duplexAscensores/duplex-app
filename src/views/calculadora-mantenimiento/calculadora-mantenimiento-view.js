import { LitElement, html } from 'lit-element';
import { style } from './calculadora-mantenimiento-style';

import { shareStyle } from '../../../node_modules/duplex-theme/src/duplex-theme';
class CalculadoraMantenimientoView  extends LitElement {

  static get styles() {
    return [
      shareStyle,
      style
    ];
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  render() {
    return html`
      
    `;
  }
}

customElements.define('calculadora-mantenimiento-view', CalculadoraMantenimientoView);