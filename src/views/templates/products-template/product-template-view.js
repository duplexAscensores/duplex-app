import { LitElement, html } from 'lit-element';
import { shareStyle } from '../../../../node_modules/duplex-theme/src/duplex-theme';

import { style } from './product-template-style';
class ProductTemplateView  extends LitElement {

  static get styles() {
    return [
      shareStyle,
      style
    ]
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <h4>product</h4>
    `;
  }
}

customElements.define('product-template-view', ProductTemplateView);