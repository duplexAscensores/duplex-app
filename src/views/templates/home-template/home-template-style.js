import { css } from 'lit-element';

const style = css `
  :host {
    display: block;
    width: 100%;
    height: 100%;
    font-family: var(--duplex-fontDefault);
    font-style: normal;
  }

  .container-hero-element {
    height: 100%;
  }

  lit-hero-element img {
    height: 550px;
  }

`;

export { style };
