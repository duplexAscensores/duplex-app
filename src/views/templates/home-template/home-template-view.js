import { LitElement, html } from 'lit-element';
import { shareStyle } from '../../../../node_modules/duplex-theme/src/duplex-theme';
import '../../../../node_modules/lit-hero-element/src/lit-hero-element';
import { style } from './home-template-style';
class HomeTemplateView  extends LitElement {

  static get styles() {
    return [
      shareStyle,
      style
    ]
  }

  
  static get properties() {
    return {};
  }

  constructor() {
    super();
    //this._setInterSectionObserver();

  }

/*   _setInterSectionObserver() {
    this.addEventListener('start-observing-intersection', (event)=> {
      const io = this._getOrCreateIntersectionObserver(event.detail.threshold);
      io.observer.observe(event.detail.element);
      io.fnMap.set(event.detail.element, event.detail.callback);
    });

    this.addEventListener('stop-observing-intersection', (event) => {
      const io = this._getOrCreateIntersectionObserver(event.detail.threshold);
      io.observer.unobserve(event.detail.element);
      io.fnMap.delete(event.detail.element);
    });
  }

  _getOrCreateIntersectionObserver(threshold = 0.99) {
    const name = '_intersectionObserver' + (threshold * 100);
    if (!this[name]) {
      this[name] = {
        observer: new IntersectionObserver(entries => {
          entries.forEach( observerEntry => {
            this[name].fnMap.get(observerEntry.target)([observerEntry])
          });
        }, { threshold: threshold}),
        fnMap: new Map()
      }
    }
    return this[name];
  } */




  render() {
    return html`
      <section class="container-hero-element">
        <lit-hero-element>
          <img slot="background-element" src="./resources/img/home-template/duplex-fondo-home.jpg"/>
        </lit-hero-element>
      </section>
    `;
  }
}

customElements.define('home-template-view', HomeTemplateView);