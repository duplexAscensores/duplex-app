import { LitElement, html } from 'lit-element';
import { style } from './service-template-style'
import { shareStyle } from '../../../../node_modules/duplex-theme/src/duplex-theme';

class ServiceTemplateView  extends LitElement {

  static get styles() {
    return [
      shareStyle,
      style
    ];
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <h4>Es Service template</h4>
    `;
  }
}

customElements.define('service-template-view', ServiceTemplateView);