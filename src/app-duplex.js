import { LitElement, html, css } from 'lit-element';
import { shareStyle } from '../node_modules/duplex-theme/src/duplex-theme';
import { initRouter } from './utils/router';
import { connect, installOfflineWatcher } from 'pwa-helpers';
import { store } from './redux/store';
import { navigate, offLineApp } from './redux/actions/actions';
import '../node_modules/lit-header-menu/src/lit-header-menu';
import  { mocksLitHeaderMenu } from './utils/duplex-app-mocks';
import { PropertyComputing } from './utils/mixin/observerProperties';


class AppDuplex  extends connect(store)(LitElement) {

  static get styles() {
    return [
      shareStyle,
      css `
        :host{
          height: 100%;
          display: block;
        }
        
        header {
          height: 56px;
        }

        main {
          height: 100%;
        }

        home-template-view {
          height: 100%;
        }

        @media only screen and (min-width: 834px) {
          header {
            height: 115px;
          }
        }
      `
    ]
  }

  static get properties() { 
    return {
      _page: { type: String },
      _titlePage: { type: String },
      _offline: { type: Boolean }
    };
  }

  firstUpdated() {
    initRouter(this);
    this._handlesListenerEventRouter();
    installOfflineWatcher( offline => store.dispatch(offLineApp(offline)))
  }

 /*  updated(changedProps) {
    if (changedProps.has('_page')) {
     if(changedProps.get('_page') !== undefined && changedProps.get('_page') !== this._page ) {
      this._createTitlePage(this._page);
     }
    } 
  } */

  _createTitlePage(page) {
    
  }
    
  constructor() {
    super();
    this.addEventListener('on-clicked-lit-header-menu-option', this._handleOnclickedSubmenuOption);
  }

  

  render() {
    return html`
      <header>
        <lit-header-menu .menuMobile="${mocksLitHeaderMenu['menuMobile']}" .menu=${mocksLitHeaderMenu['menu']} .logo="${mocksLitHeaderMenu['logo']}" .icontlf="${mocksLitHeaderMenu['iconTlf']}"></lit-header-menu>
      </header>
      <main>

      </main>
      <footer>
        
      </footer>
    `;
  }

  stateChanged(state) {
    this._page = state.currentUrl;
    this._offline = state.offLineApp;
  }

  _handlesListenerEventRouter() {
    window.addEventListener('vaadin-router-location-changed', ({detail}) => {
      store.dispatch(navigate(detail.location.pathname));
    });
  }

  _handleOnclickedSubmenuOption({detail}) {
    window.location.replace(`${window.location.origin}${detail.newUrl}`);
  }

}

window.customElements.define('app-duplex', AppDuplex);
