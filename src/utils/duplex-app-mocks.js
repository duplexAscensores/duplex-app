const mocksLitHeaderMenu = {
  logo: {
    mobile: './resources/img/logo_duplex_negativo_vectorial.svg',
    desktop: './resources/img/icons/LogoDuplexVectorial.svg'
  },
  iconTlf: './resources/img/icons/tlf.svg',
  menuMobile: [
    'Mantenimiento',
    'Nueva Instalación',
    'Ascensores',
    'Calculadora de mantenimiento',
    'Diseña tu Ascensor'
  ],
  menu: {
    Servicios: [
      {
        submenu: 'Mantenimiento',
        url: '/servicios/mantenimiento',
        icon: 'resources/img/icons/mantenice-icon.svg'
      },
      {
        submenu: 'Nueva instalación',
        url: '/servicios/nueva-instalacion',
        icon: 'resources/img/icons/elevator-icon.svg'
      },
      {
        submenu: 'Modernización',
        url: '/servicios/modernizacion',
        icon: 'resources/img/icons/modern-icon.svg'
      },
      {
        submenu: 'Accesibilidad', 
        url: '/servicios/accesibilidad',
        icon: 'resources/img/icons/accesibility-icon.svg'
      }
    ],
    Productos: [
      {
        submenu: 'Ascensores',
        url: '/productos/ascensores',
        icon: 'resources/img/icons/elevator-single-icon.svg'
      },
      {
        submenu: 'Ascensores industríales',
        url: '/productos/industriales',
        icon: 'resources/img/icons/elevator-icon.svg'
      },
      {
        submenu: 'Salvaescaleras',
        url: '/productos/salvaescaleras',
        icon: 'resources/img/icons/lit-chair-icon.svg'
      },
      {
        submenu: 'Puertas de garaje',
        url: '/productos/puertas-garaje',
        icon: 'resources/img/icons/garaje-icon.svg'
      },
      {
        submenu: 'Escaleras mecánicas',
        url: '/productos/escaleras-mecanicas',
        icon: 'resources/img/icons/mechanical-stair-icon.svg'
      }
    ],
    Herramientas: [
      {
        submenu: 'Calculadora mantenimiento',
        url: '/herramientas/calculadora-mantenimiento',
        icon: 'resources/img/icons/calculator-icon.svg'
      },
      {
        submenu: 'Diseña tu ascensor',
        url: '/herramientas/disena-ascensor',
        icon: 'resources/img/icons/desing-icon.svg'
      }
    ],
    Nosotros: [
      {
        submenu: 'Quienes somos',
        url: '/quienes-somos',
        icon: 'resources/img/icons/isotipo-duplex.svg'
      },
      {
        submenu: 'Trabaja con nosotros',
        url: '/trabaja-nosotros',
        icon:  'resources/img/icons/work-icon.svg'
      }
    ],
    Contacto: [
      '/contacto'
    ]
  }
}

export { mocksLitHeaderMenu };
