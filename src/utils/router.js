import { Router } from '@vaadin/router';
import '../views/templates/home-template/home-template-view';
import { urlPath } from './routerPath';


export const initRouter = (element) => {
  const router = new Router(element.shadowRoot.querySelector('main'));
  router.setRoutes([
    ...router.getRoutes(),
    {
      path: '/',
      animate: true,
      component: 'home-template-view'
    },
    {
      path: '/home',
      animate: true,
      component: 'home-template-view'
    },
    ...urlPath
  ]);
};
