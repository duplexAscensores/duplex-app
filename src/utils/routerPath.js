export const urlPath = [
  {
    path: '/servicios/mantenimiento',
    component: 'service-template-view',
    action: () => 
      import('../views/templates/services-template/service-template-view')
  },
  {
    path: '/servicios/nueva-instalacion',
    component: 'service-template-view',
    action: () => 
      import('../views/templates/services-template/service-template-view')
  },
  {
    path: '/servicios/modernizacion',
    component: 'service-template-view',
    action: () => 
      import('../views/templates/services-template/service-template-view')
  },
  {
    path: '/servicios/accesibilidad',
    component: 'service-template-view',
    action: () => 
      import('../views/templates/services-template/service-template-view')
  },
  {
    path: '/productos/ascensores',
    component: 'product-template-view',
    action: () => 
      import('../views/templates/products-template/product-template-view')
  },
  {
    path: '/productos/industriales',
    component: 'product-template-view',
    action: () => 
      import('../views/templates/products-template/product-template-view')
  },
  {
    path: '/productos/salvaescaleras',
    component: 'product-template-view',
    action: () => 
      import('../views/templates/products-template/product-template-view')
  },
  {
    path: '/productos/puertas-garaje',
    component: 'product-template-view',
    action: () => 
      import('../views/templates/products-template/product-template-view')
  },
  {
    path: '/productos/escaleras-mecanicas',
    component: 'product-template-view',
    action: () => 
      import('../views/templates/products-template/product-template-view')
  },
  {
    path: '/herramientas/calculadora-mantenimiento',
    component: 'calculadora-mantenimiento-view',
    action: () => 
      import('../views/calculadora-mantenimiento/calculadora-mantenimiento-view')
  },
  {
    path: '/herramientas/disena-ascensor',
    component: 'disena-ascensor-view',
    action: () => 
      import('../views/disena-ascensor/disena-ascensor-view')
  },
];