export const PropertyComputing = (base) => class extends base {
  update(changedProperties) {
    Object.entries(this.constructor.properties)
      .filter(([key, value]) => typeof value.compute === 'string')
      .forEach(([key, value]) => {
        if(value.deps !== undefined) {
          if(value.deps.some(k => changedProperties.has(k))) {
            this[key] = this[value.compute](...value.deps.map(dep => this[dep]));
          }
        }
      })
    super.update(changedProperties);
  }
};
